#include "ros/ros.h"
#include "std_msgs/String.h"
#include "geometry_msgs/PoseStamped.h"

void chatterCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  ROS_INFO_STREAM("Received pose:" << msg);
  double x_current = msg->pose.position.x;
  double y_current = msg->pose.position.y;
  ROS_INFO_STREAM(x_current);
  ROS_INFO_STREAM(y_current);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "my_ros_subscriber");
  ros::NodeHandle nh;

  ros::Subscriber sub = nh.subscribe("/ATRV/pose", 1000, chatterCallback);

  ros::spin();

  return 0;
}
