#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export LD_LIBRARY_PATH="/opt/ros/indigo/lib/typelib:/home/etdisc/Bureau/catkin_ws/devel/lib:/opt/ros/indigo/lib:/opt/ros/indigo/lib/typelib:/opt/ros/indigo/lib/typelib:/opt/openrobots/lib/"
export LUA_CPATH=";;/opt/openrobots/lib/?.so;;/opt/ros/indigo/lib/lua/5.1/?.so;/opt/ros/indigo/lib/lua/5.1/?.so;/opt/ros/indigo/lib/lua/5.1/?.so"
export LUA_PATH=";;/opt/openrobots/share/lua/5.1/?.lua;/opt/openrobots/bin/?.lua;/opt/openrobots/lib/lua/rfsm/?.lua;/usr/local/share/lua/?.lua;;/opt/ros/indigo/share/lua/5.1/?.lua;/opt/ros/indigo/share/lua/5.1/?.lua;/opt/ros/indigo/share/lua/5.1/?.lua"
export PWD="/home/etdisc/Bureau/catkin_ws/build"